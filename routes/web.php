<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});
Route::get('/', 'CreatesController@showData');
Route::get('/home', 'CreatesController@showData');
Route::get('/create', 'CreatesController@create');
Route::post('/create', 'CreatesController@insert');
Route::get('/read/{id}', 'CreatesController@read');
Route::get('/update_page/{id}', 'CreatesController@update_page');
Route::post('/update', 'CreatesController@update');
Route::get('/delete/{id}', 'CreatesController@delete');
Route::post('/search', 'CreatesController@search');