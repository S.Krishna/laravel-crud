<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;

class CreatesController extends Controller
{
    public function showData()
    {
    	$articles = Article::all();
    	return view('crud', ['articles'=>$articles]);
    }
    public function create()
    {
    	return view('create');
    }
    public function insert(Request $req)
    {
    	$this->validate($req, [
    		'title'=> 'required',
    		'description'=> 'required'
    	]);
    	$article = new Article;
    	$article->title = $req->input('title');
    	$article->description = $req->input('description');
    	$article->save();
    	return redirect('/create')->with('result', 'The new post was added successfully!');
    }
    public function read($id)
    {
    	$item = Article::find($id);
    	return view('read_post', ['item'=>$item]);
    }
   	public function update_page($id)
   	{
   		$item = Article::find($id);
    	return view('update_post', ['item'=>$item]);
   	}
    public function update(Request $req)
    {
    	$this->validate($req, [
    		'title'=> 'required',
    		'description'=> 'required'
    	]);
    	$newData = [
    		'title'=> $req->input('title'),
    		'description'=> $req->input('description')
    	];
    	Article::where('id', $req->input('id'))->update($newData);
    	return redirect('/home')->with('update_result', 'The post with ID ' . $req->input('id') . ' was successfully Updated.');
    }
    public function delete($id)
    {
    	Article::where('id', $id)->delete();
    	return redirect('/home')->with('delete_result', 'The post with ID ' . $id . ' was successfully Deleted.');
    }
    public function search(Request $req)
    {
    	echo $req->input('title') . '<br>';
    	$item = Article::where('title', $req->input('title'))->select('*');
    	var_dump($item);
    }
}
