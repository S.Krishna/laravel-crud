@include('inc.header')

<div class="row">
    <div class="col-xl-1"></div>
    <div class="col-xl-10"><br>
        <div class="jumbotron">
        <h1 class="display-3">{{ $item->id }}. {{ $item->title }}</h1>
        <p class="lead">{{ $item->description }}</p>
        <hr class="my-4">
        <p class="lead">
          <a class="btn btn-primary btn-lg" href="/public/" role="button">All Posts</a>
        </p>
      </div>
    </div>
    <div class="col-xl-1"></div>
</div>

@include('inc.footer')