@include('inc.header')

<div class="row">
	<div class="col-xl-1"></div>
	<div class="col-xl-10"><br>
      @if (count($errors)>0)
          @foreach ($errors->all() as $error)
            <div class="alert alert-danger">
              {{ $error }}
            </div>
          @endforeach
      @endif
  		<form action="/public/update" method="post">
        @csrf
      <fieldset>
        <legend>Update Post</legend>
        <div class="form-group">
          <label for="id">ID</label>
          <input name="id" type="number" class="form-control" id="id" placeholder="Post ID" 
          @if(isset($item->id)) value="{{ $item->id }}"> @endif
        </div>
        <div class="form-group">
          <label for="title">Title</label>
          <input name="title" type="text" class="form-control" id="title" placeholder="Post Title" value="@if(isset($item->title)) {{ $item->title }} @endif">
        </div>
        <div class="form-group">
          <label for="description">Description</label>
          <textarea name="description" class="form-control" id="description" rows="3" placeholder="Post Description">@if(isset($item->description)) {{ $item->description }} @endif</textarea>
        </div>
        </fieldset>
        <button type="submit" class="btn btn-primary">Update The Post</button>
      </fieldset>
      </form>
	</div>
	<div class="col-xl-1"></div>
</div>


@include('inc.footer')