@include('inc.header')

<div class="row">
	<div class="col-xl-1"></div>
	<div class="col-xl-10"><br>
      @if (count($errors)>0)
          @foreach ($errors->all() as $error)
            <div class="alert alert-danger">
              {{ $error }}
            </div>
          @endforeach
      @endif
      @if (session('result'))
          <div class="alert alert-primary">
            {{ session('result') }}
          </div>
      @endif
  		<form action="/public/create" method="post">
        @csrf
      <fieldset>
        <legend>New Post</legend>
        <div class="form-group">
          <label for="title">Title</label>
          <input name="title" type="text" class="form-control" id="title" placeholder="Post Title">
        </div>
        <div class="form-group">
          <label for="description">Description</label>
          <textarea name="description" class="form-control" id="description" rows="3" placeholder="Post Description"></textarea>
        </div>
        </fieldset>
        <button type="submit" class="btn btn-primary">Add New Post</button>
      </fieldset>
      </form>
	</div>
	<div class="col-xl-1"></div>
</div>


@include('inc.footer')