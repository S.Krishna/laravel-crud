<!DOCTYPE html>
<html>
<head>
	<title>Laravel CRUD</title>
	<link rel="stylesheet" type="text/css" href="/public/css/bootstrap.min.css">
	<script type="text/javascript" src="/public/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/public/js/jquery-3.2.1.slim.min.js"></script>
	<script type="text/javascript" src="/public/js/popper.min.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">Laravel CRUD</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/public/">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/public/create">Create</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/public/update_page/0">Update</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0" action="/public/search" method="post">
      @csrf
      <input name="title" class="form-control mr-sm-2" type="text" placeholder="Search">
      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>