@include('inc.header')

<div class="row">
	<div class="col-xl-1"></div>
	<div class="col-xl-10"><br>
      @if (session('update_result'))
          <div class="alert alert-primary">
            {{ session('update_result') }}
          </div>
      @endif
      @if (session('delete_result'))
          <div class="alert alert-warning">
            {{ session('delete_result') }}
          </div>
      @endif
		<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Title</th>
      <th scope="col">Description</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      @if(count($articles)>0)
        @foreach($articles as $article)
          <tr class="table-primary">
              <th scope="row">{{ $article->id }}</th>
              <td>{{ $article->title }}</td>
              <td>{{ $article->description }}</td>
              <td>
                	<a href="/public/read/{{ $article->id }}"><button type="button" class="btn btn-primary">Read</button></a> | 
                	<a href="/public/update_page/{{ $article->id }}"><button type="button" class="btn btn-success">Update</button></a> | 
          		    <a href="/public/delete/{{ $article->id }}"><button type="button" class="btn btn-danger">Delete</button></a>
              </td>
          </tr>
        @endforeach
      @endif
  </tbody>
</table> 
	</div>
	<div class="col-xl-1"></div>
</div>


@include('inc.footer')